package com.te

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.extensions.kotest.annotation.MicronautTest

@MicronautTest
class HelloControllerTest(private val embeddedServer: EmbeddedServer, private val client: HttpClient) : BehaviorSpec({
    given("a hello endpoint") {
        `when`("we hit /hello") {
            val resp: String = client.toBlocking().retrieve("/ruok")
            then("we should get back plain text with a spanish greeting") {
                resp shouldBe "Hola!"
            }
        }
    }

})